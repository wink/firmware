#ifndef APP_CONFIG_H
#define APP_CONFIG_H

/**
 * @brief If defined, enables scanning for extended advertisements
 * as specified by BLE 5.0.
 * Currently supports mode 00 (non-scannable, non-connectible) */
//#define USE_EXTENDED_ADV
#undef USE_EXTENDED_ADV

#endif /* APP_CONFIG_H */
